
Summary to build and install C++ libraries and applications using CMake, Conan, Gcc, MinGW, MSVC.

[[_TOC_]]

# Install tools and libraries

## Git

TODO: document

## CMake

[CMake home page](https://cmake.org)

### Install CMake on Linux

The CMake shipped with recent distributions is fine.

On Ubuntu:
```bash
sudo apt-get install cmake
```

To install a more recent version of CMake on Ubuntu,
you should look at the [Kitware APT Repository](https://apt.kitware.com) .

### Install CMake on Windows

Choose a installer that matches your platform from the [Download](https://cmake.org/download/) section.
Here are some options that I choosed (all other I keeped default):
* "Add CMake to the system PATH for the current user"

## Sphinx

Sphinx is mainly used to generate CMake documentation.

See the [Sphinx homepage](https://www.sphinx-doc.org)

### Install Sphinx on Linux

On Ubuntu 18.04:
```bash
sudo apt-get install python3-sphinx
```

## Doxygen

Doxygen is used to generate C++ API documentations.

See the [Doxygen homepage](http://www.doxygen.nl)

### Install Doxygen on Linux

On Ubuntu 18.04:
```bash
sudo apt-get install doxygen graphviz
```


## Conan

[Conan home page](https://conan.io)


### Install Conan on Linux

It is recommanded to install Conan with PyPI and use Python 3.

On Ubuntu 16.04, I had problems with Python 3,
so I had to install a old version of Conan.

On Ubuntu 18.04, install PyPI for Python 3:
```bash
sudo apt-get install python3-pip
```

Follow the [Conan install documentation](https://docs.conan.io/en/latest/installation.html)
 and replace pip commands by pip3 .


### Install Conan on Windows

Conan recommends at least Python 3.5.

See [Conan install documentation](https://docs.conan.io/en/latest/installation.html)
for some notes.

Some Windows computers comes with Python allready installed.
See [Python For Beginners](https://www.python.org/about/gettingstarted)
for more informations.

On Windows 10, try to launch the Windows Start menu and type `python`.
If not allready installed, it should launch the Microsoft Store.
In my case, this ended with a error.

Go to the [python for Windows download page](https://www.python.org/downloads/windows)
Choose the latest stable version that runs on your machine, and that provides a installer.
Choose your installer, for example `Windows x86 executable installer` .

Note: for a discution about choosing x86 or x86_64,
see the [BeginnersGuide wiki](https://wiki.python.org/moin/BeginnersGuide/Download).

Launch the installer.
For simplicity, check the option to add python to the PATH.

Open a terminal (`cmd`) and and check that pip runs and refers to the correct version of python:
```bash
pip --version
```

Install Conan:
```bash
pip install conan
```

Follow the [Conan install documentation](https://docs.conan.io/en/latest/installation.html)
to setup conan, notably the Initial configuration.

### Add Conan remotes

Some dependencies are not available on official Conan servers.

Add the Bincrafters remote:
```bash
conan remote add bincrafters https://bincrafters.jfrog.io/artifactory/api/conan/public-conan
```

Add scandyna remote:
```bash
conan remote add scandyna https://gitlab.com/api/v4/projects/25668674/packages/conan
```

## Compiler and build tools

To create libraries and applications using C++,
 a compiler that support C++-14 is required.
Allmost recent version of [GCC](https://gcc.gnu.org) or [Clang](https://clang.llvm.org) are totally fine.

### Install Gcc and make on Linux

On Ubuntu (18.04):
```bash
sudo apt-get install g++ make
```

Some builds can require Gcc 8.
On Ubuntu (18.04):
```bash
sudo apt-get install g++-8 make
```


### Install Clang on Linux

On Ubuntu (18.04):
```bash
sudo apt-get install clang libclang-dev libc++-dev libc++abi-dev
```

### Install Gcc and Make on Windows

My projects mostly uses Qt5, which ships Gcc and Make,
 so this topic will be covered later.

To install Gcc without Qt5, see [GCC for Windows 64 & 32 bits](http://mingw-w64.org)


### Install MSVC on Windows

Download the [Windows 10 SDK](https://developer.microsoft.com/en-us/windows/downloads/windows-10-sdk)
installer and launch it.
Choose your options and start installation.
I choosed the default installation path and installed all components,
but this could probably be more fine graded.

The following section will describe how to use a terminal with a environment set.
It seems not to be required when using CMake with a Visual Studio generator.
It could potentially help to run some executable directly in the terminal,
or to use the NMake generator.

In the Windows menu you should find a item (folder) called "Visual Studio 201x".
Below it some Command Prompt should be available.
When launched, a batch file will be called to setup a build environment.

In my case, Windows 10 machine with the SDK for Visual Studio 2017,
the shortcuts are located in C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Visual Studio 2017\Visual Studio Tools\VC .
The batch files that are most interesting are in C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build .

In my case, I want to be in my developement directory once the terminal was launched.
I made a copy of the 2 interesting files, 'vcvars32.bat' and 'vcvars64.bat', and edited them.

Here is the content of vcvars32.bat:
```batch
call "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" x86
"C:\WINDOWS\system32\cmd.exe"
```

The vcvars64.bat has only the argument that changes:
```batch
call "C:\Program Files (x86)\Microsoft Visual Studio\2017\BuildTools\VC\Auxiliary\Build\vcvarsall.bat" x64
"C:\WINDOWS\system32\cmd.exe"
```

Check that MSBuild is available:
```cmd
msbuild /version
```

Some interesting explanations on using MSVC with CMake can be found
[here](https://dmerej.info/blog/post/cmake-visual-studio-and-the-command-line/).


## Qt5

[Qt5 home page](https://www.qt.io)

### Install Qt5 provided by Ubuntu

If your distribution provides the required version, you can use that.
For example, on Ubuntu:
```bash
sudo apt-get install qtbase5-dev qtbase5-dev-tools libqt5gui5 libqt5network5 libqt5sql5 libqt5sql5-mysql libqt5sql5-psql libqt5sql5-sqlite libqt5test5 libqt5widgets5
```

For Ubuntu 18.04 some additionnal packages are required:
```bash
sudo apt install qttools5-dev qtbase5-private-dev
```

Optionnally, documentation can also be installed:
```bash
sudo apt-get install qtbase5-doc qtbase5-doc-html qtbase5-examples
```

### Install Qt5 with Qt installer on Linux

It's also recommanded to have a look at the [documentation](http://doc.qt.io)
in the "Getting Started Guides" section.

Maybe you will have to create a [Qt Acount](https://account.qt.io)

At first, install [requirements](http://doc.qt.io/qt-5/linux.html) .

Then, you can get Qt5 from the [Download section](https://www.qt.io/download) .

We must make the installer executable and run it.
In my case, I have put the installer to ~/opt/qt/installers:
```bash
cd ~/opt/qt/installers
chmod u+x qt-unified-linux-x64-3.0.0-online.run
./qt-unified-linux-x64-3.0.0-online.run
```
Of course, the name of the installer can be different in your case.

Follow the wizzard.
In my case, I choosed to install Qt5 to ~/opt/qt/Qt5

### Install Qt5 with sanitizers

Some sanitizers, like [ThreadSanitizer](https://github.com/google/sanitizers/wiki/ThreadSanitizerCppManual),
requires that Qt itself is built with the sanitizer is question.

Qt does not provide binaries compiled with sanitizers support.

A solution is to use Conan packages.

[The Bincrafters conan-qt](https://github.com/bincrafters/conan-qt)
provides Qt packages, but no option to enable sanitizers.

I try to provide sanitizers option in the Bincrafters recipe.
For available Qt builds, see [my conan-qt-builds](https://gitlab.com/scandyna/conan-qt-builds) repository.

### Install Qt5, Gcc and Make on Windows

It's also recommanded to have a look at the [documentation](http://doc.qt.io)
in the "Getting Started Guides" section.

Maybe you will have to create a [Qt Acount](https://account.qt.io)

Qt5 can be downloaded from [here](https://www.qt.io/download/).
Check that the proposed installer matches your platform and start the download.
In my case, a online installer was selected.

If you allready installed Qt5 on your machine,
you could run the Qt maintenance tool directly.

Run the installer.
You probably will have to login with your acount created before.
It will be asked about the installation path, which we have to remember for a later step.
For the rest of this document, C:\Qt will be considered.
It's also possible to select components to install.
The default should be fine.

Once installation is done, a Qt entry should be added in the Start/Windows menu.
Bellow it, execute the shortcut called "Qt x.y.z for Desktop".
A window with a command prompt should pop-up.
From this prompt, all that is needed to compile Qt applications is in the PATH.

Check gcc version:
```bash
gcc --version
```
It should return a version at least 5.0.0 , in which case C++14 support is good.

Check make version:
```bash
mingw32-make --version
```
Check that the excepted architecture is shown.
Note that the make executable is called mingw32-make
for X86 (32bit) and X86_64 (64bit).


Check Qt installation:
```bash
qmake -version
```
Check that the expected Qt library is used.

# Some links

[Awesome CMake](https://github.com/onqtam/awesome-cmake)
